﻿using System;

namespace IsUnique
{
    class Program
    {
        //this challenge is from 'Cracking the coding interview', 6th edition
        //challenge is - Is Unique.
        //implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?

        //new method, defined as a static string as it will return the result in a form of a string
        //argument is a string
        static string IsUniqueCharacters(string str)
        {
            //takes the passed in argument and converts it to a char array
            char[] array = str.ToCharArray();

            //now we sort the array - this sorts it into a one dimensional array
            Array.Sort(array);

            //for loop, as long as i is less than the array length - do the following (the increment by 1)
            for (int i = 0; i < array.Length -1; i++)
            {
                //if the adjacent element are NOT equal - move onto the next element!
                if (array[i] != array[i + 1])
                    //passes control to the next iteration of the for loop
                    continue;
                else
                    //if any character is not unique - return the following string
                    return "Not unique!";
            }
            //if all characters in the array are unique, return the following string
            return "Unique!";
        }
        static void Main(string[] args)
        {
            Console.Title = "Is Unique";
            Console.WriteLine("Coding challenge - Is Unqiue");
            Console.WriteLine(IsUniqueCharacters("New"));
            Console.WriteLine(IsUniqueCharacters("test"));
            Console.WriteLine(IsUniqueCharacters("AAAAA"));
            Console.WriteLine(IsUniqueCharacters("ABCD"));
        }
    }
}
