﻿using System;

//this challenge is from 'Cracking the coding interview', 6th edition
//challenge is - String Compression
//Implement a method to perform basic string compression using the counts of repeated characters.
//i.e. aabccccaaa would be a2b1c5a3
//if the compressed string isn't smaller than the original string - return the original string
//we can assume the string has uppercase and lowercase letters

namespace StringCompression
{
    class Program
    {
        //output string defined
        public static string compressedString;
        public static int stringCompress(string input)
        {
            //count integer defined
            int count = 0;
            //take the input into a character array
            char[] array = input.ToCharArray();
            Console.WriteLine(array);

            //increment through each element
            for (int i = 0; i <= array.Length; i++)
            {
                Console.WriteLine(array);
            }
            return count;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("String Compression");
            Console.WriteLine(stringCompress("aaabbc"));
        }
    }
}
