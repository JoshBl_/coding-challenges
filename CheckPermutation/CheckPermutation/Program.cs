﻿using System;

namespace CheckPermutation
{
    class Program
    {
        //this challenge is from 'Cracking the coding interview', 6th edition
        //challenge is - Check Permutation
        //given two strings, write a method to decide if one is a permutation of the other

        //permutation method - takes a string as an argument
        //int s = start of index
        //int e = end of index
        private static void permutation (String str, int s, int e)
        {
            //if s and e are equal, print the string
            if (s == e)
            {
                Console.WriteLine(str);
            }
            else
            {
                //as long as i is less than or equal to the length of e, run the following 
                for (int i = s; i <= e; i++)
                {
                    str = swap(str, s, i);
                    //recursion - take the current value of the string and integers
                    permutation(str, s + 1, e);
                    str = swap(str, s, i);
                }
            }
        }
        
        //swap characters at postion
        //a = string 
        //i = position 1
        //j = position 2
        //this method will return a swapped string
        public static String swap (String a, int i, int j)
        {
            char temp;
            //new char array created based on input string
            char[] charArray = a.ToCharArray();
            //temp value is a letter of an array (int i)
            temp = charArray[i];
            //the letter held within i is swapped with j
            charArray[i] = charArray[j];
            //the letter held with j is swapped with the temp char variable
            charArray[j] = temp;
            //a new string is created based on the swap
            string s = new string(charArray);
            //return new string
            return s;
        }

        static void Main(string[] args)
        {
            Console.Title = "Check Permutation";
            Console.WriteLine("Check Permutation");

            //new string
            String str = "ABC";
            //integer value is length of string (so 3 in this case)
            int n = str.Length;
            //call method
            permutation(str, 0, n - 1);
        }
    }
}
